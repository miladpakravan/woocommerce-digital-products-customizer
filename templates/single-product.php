<?php
/**
 * Single Product file box template
 *
 * @package Woodpc
 * @since 1.0.0
 */
?>
<?php if ( ! defined( 'ABSPATH' ) ) : ?>
    <p><?php esc_html_e( 'You are not authorized to view downloadable files', 'woodpc' ); ?></p>
    <?php die(); ?>
<?php endif; ?>
<?php global $wp_query; ?>
<?php if ( isset( $wp_query->query_vars['woodpc_data'] ) ) : ?>
    <?php $woodpc_data = $wp_query->query_vars['woodpc_data']; ?>
    <?php $topics = $woodpc_data->topics; ?>
    <?php else : ?>
        <p><?php esc_html_e( 'Error occures while get downloadable files', 'woodpc' ); ?></p>
        <?php die(); ?>
    <?php endif; ?>
<div class="woodpc">
<?php for ( $topic_id = 0; $topic_id < count( $topics ); $topic_id++ ) : ?>
    <?php if ( isset( $woodpc_data->files_meta[ $topic_id ] ) ) : ?>
        <?php $topic_name = $topics[ $topic_id ]; ?>
        <?php $files_meta = $woodpc_data->files_meta[ $topic_id ]; ?>
        <div class="topic-box">
            <span>
                <?php echo esc_html( $topic_name ); ?>
            </span>
        </div>
        <div class="box-accordion">
            <?php for ( $position = 0; $position < count( $files_meta ); $position++ ) : ?>
                <?php $file_meta = $files_meta[ $position ]; ?>
            <div class="file-box">
                <div class="title">
                    <span>
                        <?php echo esc_html( $file_meta['name'] ); ?>
                    </span>
                </div>
                <div class="detail">
                    <span>
                        <?php echo esc_html( $file_meta['attribute'] ); ?>
                    </span>
                    <?php if ( true === $file_meta['download_status'] ) : ?>
                        <span class="download-button">
                            <a href="<?php echo esc_url( $file_meta['url'] ); ?>" target="_blank">
                                <?php esc_html_e( "Download", 'woodpc' ); ?>
                            </a>
                        </span>
                    <?php else : ?>
                        <span class="buy-button">
                            <a href="<?php echo esc_url( $file_meta['url'] ); ?>">
                                <?php esc_html_e( "Buy", 'woodpc' ); ?>
                            </a>
                        </span>
                    <?php endif; ?>
                </div>
            </div>
            <?php endfor; ?>
        </div>
    <?php endif; ?>
<?php endfor; ?>
</div>

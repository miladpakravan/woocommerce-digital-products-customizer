jQuery(document).ready(function ($) {
  $(".woodpc").accordion({
    collapsible: true,
    heightStyle: "content",
  });

  $( ".woodpc .title" ).on("click", function(){
    let width = $(window).width();
    if ( width <= 767 ) {
      let detail_tag = $( this ).next( ".detail" );
      if ( detail_tag.css( 'display' ) === 'none' ) {
        detail_tag.css( 'display', 'flex' );
      }else {
        detail_tag.css( 'display', 'none' );
      }
    }
  });
});
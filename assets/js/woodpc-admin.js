jQuery( function( $ ) {
    // auto generate hash for new files using wp ajax.
    function generate_file_hash() {
        $( "#woodpc-metabox .topic-box .file-hash" ).each( function() {
            if( ! $( this ).val() ) {
                hash_field = $( this );
                let data = { action: 'woodpc_generate_hash' };
                $.post(ajaxurl, data, function(response) {
                    if(response.status == "success") {
                        hash_field.val( response.hash );
                    }
                });
                return false;
            }
        } );
    }

    // woodpc metabox appreance state settings.
    function woodpc_metabox_appreance_state() {
        if( $( '#_downloadable' ).is(":checked") ) {
            $( '#woodpc-metabox' ).show();
        } else {
            $( '#woodpc-metabox' ).hide();
        }
    }

    $(document).ready(function($) {
        // if we are in woocommerce edit product page.
        if ( $('#woocommerce-product-data').length > 0 ) {
            // remove woocommerce downloadable table for prevent conflict.
            $(".downloadable_files").remove();
            // sort button functionality.
            $( '#woodpc-metabox' ).sortable({
                items: 'tbody tr',
                cursor: 'move',
                axis: 'y',
                handle: '.sort',
                scrollSensitivity: 40,
                helper: function( event, ui ) {
                    ui.children().each( function() {
                        $( this ).width( $( this ).width() );
                    });
                    ui.css( 'left', '0' );
                    return ui;
                },
                start: function( event, ui ) {
                    ui.item.css( 'background-color', '#f6f6f6' );
                },
                stop: function( event, ui ) {
                    ui.item.removeAttr( 'style' );
                    ui.item.trigger( 'updateMoveButtons' );
                    // update file row topic id when move
                    let new_topic_id = $( ui.item ).closest( ".topic-box" ).find(".topic-header input").data("topic-id");
                    let attribute = $( ui.item ).find("td input.file-attribute")[0];
                    let access_type = $( ui.item ).find("td input.file-access-type");
                    $( attribute ).attr("name", '_wc_file_attributes[' + new_topic_id + '][]' );
                    // first element for basic checkbox
                    $( access_type[0] ).attr("name", '_wc_file_access_types[' + new_topic_id + '][]' );
                    // second element for hidden checkbox
                    $( access_type[1] ).attr("name", '_wc_file_access_types[' + new_topic_id + '][]' );
                }
            });

            // auto generate file hash in startup
            generate_file_hash();

            woodpc_metabox_appreance_state();

            // fire when downloadable checkbox state changed
            $( '#_downloadable' ).on( 'change', woodpc_metabox_appreance_state );

            // Topic inputs
            $( '#woodpc-metabox .add-topic-button' ).on( 'click', function() {
                $( this ).closest( '#woodpc-metabox' ).find('.new-topic').before( $( this ).attr( 'data-row' ) );
                generate_file_hash();
                // update data-topic-id
                let data_row = $( this ).attr("data-row");
                let current_topic_id = $( data_row ).find(".topic-name").data("topic-id");
                $( this ).attr("data-row", data_row.replaceAll( 'data-topic-id="' + current_topic_id + '"', 'data-topic-id="' + ( current_topic_id + 1 ) + '"' ) );
                return false;
            } );

            // Fire when click on Remove for removing Topic and its Files
            $( '#woodpc-metabox' ).on( 'click','.topic-box .delete-topic',function() {
                if (confirm( woodpc_ajax_object.remove_topic_prompt )) {
                    $( this ).closest( '.topic-box' ).remove();
                }
                return false;
            } );

            // File inputs.
            $( '#woodpc-metabox' ).on( 'click','.topic-box .add-file-button', function() {
                $( this ).closest( '.topic-box' ).find( 'tbody' ).append( $( this ).data( 'row' ) );
                generate_file_hash();
            } );

            // File Delete
            $( '#woodpc-metabox' ).on( 'click','.topic-box .delete-file',function() {
                $( this ).closest( 'tr' ).remove();
                return false;
            });

            // Uploading files.
            $( '#woodpc-metabox' ).on( 'click', '.upload-file-button', function( event ) {
                let $el = $( this );
                let downloadable_file_frame;
                let file_path_field;

                file_path_field = $el.closest( 'tr' ).find( '.file-url' );

                event.preventDefault();

                // If the media frame already exists, reopen it.
                if ( downloadable_file_frame ) {
                    downloadable_file_frame.open();
                    return;
                }

                let downloadable_file_states = [
                    // Main states.
                    new wp.media.controller.Library({
                        library:   wp.media.query(),
                        multiple:  true,
                        title:     $el.data('choose'),
                        priority:  20,
                        filterable: 'uploaded'
                    })
                ];

                // Create the media frame.
                downloadable_file_frame = wp.media.frames.downloadable_file = wp.media({
                    // Set the title of the modal.
                    title: $el.data('choose'),
                    library: {
                        type: ''
                    },
                    button: {
                        text: $el.data('update')
                    },
                    multiple: true,
                    states: downloadable_file_states
                });

                // When an image is selected, run a callback.
                downloadable_file_frame.on( 'select', function() {
                    let file_path = '';
                    let selection = downloadable_file_frame.state().get( 'selection' );

                    selection.map( function( attachment ) {
                        attachment = attachment.toJSON();
                        if ( attachment.url ) {
                            file_path = attachment.url;
                        }
                    });

                    file_path_field.val( file_path ).trigger( 'change' );
                });

                // Set post to 0 and set our custom type.
                downloadable_file_frame.on( 'ready', function() {
                    downloadable_file_frame.uploader.options.uploader.params = {
                        type: 'downloadable_product'
                    };
                });

                // Finally, open the modal.
                downloadable_file_frame.open();
            });

            // access type checkbox event.
            $( '#woodpc-metabox' ).on( 'click','.topic-box .file-access-type',function() {
                if( $( this ).is(":checked") ) {
                    $( this ).next().remove();
                } else {
                    $( this ).after( '<input type="hidden" class="file-access-type" name="' + $(this).attr("name") + '" value="0" checked />' );
                }
            });
        } else if( $('.woodpc-settings').length > 0 ) {
            // toggle accordion-position
            $( '#accordion-position-status' ).change( function() {
                if( $( this ).is(":checked") ) {
                    $( '#accordion-position' ).prop( "disabled", false );
                } else {
                    $( '#accordion-position' ).prop( "disabled", true );
                }
            } );
        }
    });
});

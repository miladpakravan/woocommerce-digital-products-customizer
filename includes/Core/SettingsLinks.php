<?php
/**
 * Plugin settings class
 *
 * @package Woodpc
 * @since 1.0.0
 */
namespace WOODPC\Core;
/**
 * a class to filter plugin settings link.
 *
 * @since 1.0.0
 */
class SettingsLinks
{
	/**
	 * register SettingsLinks class.
	 *
	 * register filters.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function register() {
		$plugin_name = WOODPC_PLUGIN_NAME;
		add_filter( "plugin_action_links_{$plugin_name}", array( $this, 'settings_link' ) );
	}

	/**
	 * filter plugin settings link in wordpress plugins url.
	 *
	 * @since 1.0.0
	 *
	 * @return array plugin links in wordpress plugins url.
	 */
	public function settings_link( $links ) {
		$settings_link = '<a href="admin.php?page=woodpc_settings">'. esc_html__( 'Settings', 'woodpc' ) .'</a>';
		array_push( $links, $settings_link );
		return $links;
	}

}

<?php
/**
 * Manage Digital Product class
 *
 * @package Woodpc
 * @since 1.0.0
 */
namespace WOODPC\Core;
/**
 * Digital Product class.
 *
 * main functions for manage digital product is here.
 *
 * @since 1.0.0
 */
class DigitalProduct
{
    /**
     * plugin database table prefix.
     *
     * @since 1.0.0
     * @var string
     */
    private $table_prefix;
    /**
     * related woocommerce product id.
     *
     * @since 1.0.0
     * @var int
     */
    private $product_id;
    /**
     * store product topics.
     *
     * @since 1.0.0
     * @var array
     */
    private $topics;
    /**
     * store product files meta.
     *
     * @since 1.0.0
     * @var array
     */
    private $files_meta;

    /**
     * default class constructor
     *
     * @since 1.0.0
     *
     * @global object $wpdb wordpress database handler.
     *
     * @param int $sanitized_product_id woocommerce product id to be used.
     *
     * @return void
     */
    public function __construct( $sanitized_product_id ) {
        global $wpdb;
        $this->table_prefix = $wpdb->prefix . 'woodpc_';
        $this->product_id   = $sanitized_product_id;
        $this->fetch_product_meta_meta();
    }

    /**
     * save product topics and files meta.
     *
     * @since 1.0.0
     *
     * @global object $wpdb wordpress database handler.
     *
     * @param array $sanitized_meta_data {
     *     @type string $topic_name the topic name
     *     @type array $files_meta {
     *         @type string $hash woocommerce file hash (file id)
     *         @type string $attribute file attribute
     *         @type int $free if file is free to download, free equal 1
     *     }
     * }
     *
     * @return void
     */
    public function store_product_meta_data( $sanitized_meta_data ) {
        global $wpdb;
        if ( current_user_can( 'manage_woocommerce' ) && is_array( $sanitized_meta_data ) ) {
            // delete product old topics and insert new topics.
            $topics_insert_id = array();
            $topics_table     = $this->table_prefix . 'topics';
            /*
             * delete product old topics.
             * by deleting product topics, files meta will be deleted cascade.
             */
            $wpdb->delete( $topics_table, array( 'product_id' => $this->product_id ) );
            for ( $topic_id = 0; $topic_id < count( $sanitized_meta_data ); $topic_id++ ) {
                $data = array(
                    'topic_id'   => $topic_id,
                    'product_id' => $this->product_id,
                    'name'       => $sanitized_meta_data[ $topic_id ]['topic_name'],
                );
                $format = array( '%d', '%d', '%s' );
                // insert product new topics.
                $wpdb->insert( $topics_table, $data, $format );
                array_push( $topics_insert_id, $wpdb->insert_id );
            }
            // insert product files meta.
            $files_table = $this->table_prefix . 'files';
            for ( $topic_id = 0; $topic_id < count( $sanitized_meta_data ); $topic_id++ ) {
                for ( $position = 0; $position < count( $sanitized_meta_data[ $topic_id ]['files_meta'] ); $position++ ) {
                    $sanitized_file_meta = $sanitized_meta_data[ $topic_id ]['files_meta'][ $position ];
                    $data = array(
                        'hash'      => $sanitized_file_meta['hash'],
                        'tid'       => $topics_insert_id[ $topic_id ],
                        'position'  => $position,
                        'free'      => absint( $sanitized_file_meta['free'] ),
                        'attribute' => $sanitized_file_meta['attribute'],
                    );
                    $format = array( '%s', '%d', '%d', '%d', '%s' );
                    $wpdb->insert( $files_table, $data, $format );
                }
            }
        }
    }

    /**
     * get product topics.
     *
     * @since 1.0.0
     *
     * @return array product topics.
     */
    public function get_topics() {
        for ( $index = 0; $index < count( $this->topics ); $index++ ) {
            // if topic names is empty, consider default name.
            if ( empty( $this->topics[ $index ] ) ) {
                $this->topics[ $index ] = __( 'Untitled Topic', 'woodpc' ) . ' ' .  ( $index + 1 );
            }
        }
        return $this->topics;
    }

    /**
     * get product files meta order by topic id.
     *
     * @since 1.0.0
     *
     * @return array product files meta.
     */
    public function get_files_meta() {
        return $this->files_meta;
    }

    /**
     * get product files meta order by file hash.
     *
     * @since 1.0.0
     *
     * @return array product files meta.
     */
    public function get_files_meta_order_by_hash() {
        $hashed_file_meta = array();
        for ( $topic_id = 0; $topic_id < count( $this->files_meta ); $topic_id++ ) {
            for ( $position = 0; $position < count( $this->files_meta[ $topic_id ] ); $position++ ) {
                $file_meta = $this->files_meta[ $topic_id ][ $position ];
                $hashed_file_meta[ $file_meta['hash'] ] = array(
                    'topic_id'  => $topic_id,
                    'position'  => $position,
                    'attribute' => $file_meta['attribute'],
                    'free'      => $file_meta['free'],
                );
            }
        }
        return $hashed_file_meta;
    }

    /**
     * get product topics and files meta.
     *
     * this function ready product topics and files meta for present in single product page.
     *
     * @since 1.0.0
     *
     * @global object $product woocommerce product class object.
     * @global object $wp wordpress class object.
     *
     * @return array|null if we are in single product page return array, otherwise return null.
     */
    public function get_product_files() {
        global $product, $wp;
        if ( is_singular( array( 'product' ) ) ) {
            /**
             * for using woodpc_customer_bought_product, woodpc_get_file_path_from_download_url functions.
             */
            require_once WOODPC_PLUGIN_DIR . '/libraries/woodpc-helper.php';
            // order files_meta by hash.
            $hashed_files_meta           = $this->get_files_meta_order_by_hash();
            $pos                         = 0;
            $this->files_meta            = [];
            // get downloadable products for customer.
            $woo_downloads               = WC()->customer->get_downloadable_products();
            // if customer hash bought product.
            if ( woodpc_customer_bought_product( $this->product_id ) && count( $woo_downloads ) !== 0 ) {
                foreach ( $woo_downloads as $woo_download ) {
                    $hash = $woo_download['download_id'];
                    if ( isset ( $hashed_files_meta[ $hash ] ) && is_array( $hashed_files_meta[ $hash ] ) ) {
                        $topic_id = $hashed_files_meta[ $hash ]['topic_id'];
                        $position = $hashed_files_meta[ $hash ]['position'];
                    } else {    // consider fresh installed plugin.
                        /*
                         * if fresh installed plugin, product has not any topics.
                         * so if there is any files, for compatibility they have default values.
                         */
                        $topic_id = 0;
                        $position = $pos;
                        $pos      = $pos + 1;
                        // populate empty files meta by default values.
                        $this->files_meta[ $topic_id ][ $position ]['attribute'] = '';
                        $this->files_meta[ $topic_id ][ $position ]['free']      = 0;
                    }
                    // if customer has bought product, file download status is true.
                    $this->files_meta[ $topic_id ][ $position ]['download_status'] = true;
                    $this->files_meta[ $topic_id ][ $position ]['name']            = $woo_download['download_name'];
                    $this->files_meta[ $topic_id ][ $position ]['attribute']       = $hashed_files_meta[ $hash ]['attribute'];
                    $this->files_meta[ $topic_id ][ $position ]['url']             = $woo_download['download_url'];
                }
            } else {
                // get product files for users who not bought product.
                $woo_downloads = $product->get_downloads();
                foreach ( $woo_downloads as $woo_download ) {
                    $hash = $woo_download['id'];
                    if ( count( $hashed_files_meta ) !== 0 ) {
                        $topic_id = $hashed_files_meta[ $hash ]['topic_id'];
                        $position = $hashed_files_meta[ $hash ]['position'];
                    } else {    // consider fresh installed plugin.
                        /*
                         * if fresh installed plugin, product has not any topics.
                         * so if there is any files, for compatibility they have default values.
                         */
                        $topic_id = 0;
                        $position = $pos;
                        $pos      = $pos + 1;
                        // populate empty files meta by default values.
                        $this->files_meta[ $topic_id ][ $position ]['attribute'] = '';
                        $this->files_meta[ $topic_id ][ $position ]['free']      = 0;
                    }
                    $this->files_meta[ $topic_id ][ $position ]['name']      = $woo_download['name'];
                    $this->files_meta[ $topic_id ][ $position ]['attribute'] = $hashed_files_meta[ $hash ]['attribute'];
                    // set file download status and url according to free field.
                    if ( isset( $hashed_files_meta[ $hash ]['free'] ) && 1 === $hashed_files_meta[ $hash ]['free'] ) {
                        $this->files_meta[ $topic_id ][ $position ]['download_status'] = true;
                        $this->files_meta[ $topic_id ][ $position ]['url']             = woodpc_get_free_file_downloadable_url( $this->product_id, $hash );
                    } else {
                        $this->files_meta[ $topic_id ][ $position ]['download_status'] = false;
                        $this->files_meta[ $topic_id ][ $position ]['url']             = site_url( $wp->request ) . '/?add-to-cart=' . $this->product_id;
                    }
                }
            }
            return $this->files_meta;
        }
        return null;
    }

    /**
     * fetch stored product topics and files meta from database.
     *
     * @since 1.0.0
     *
     * @global object @wpdb wordpress database handler.
     *
     * @return void
     */
    private function fetch_product_meta_meta() {
        global $wpdb;
        // fetch topics from database.
        $this->topics = array();
        $topics_table = $this->table_prefix . 'topics';
        $sql  = $wpdb->prepare( "
            SELECT
                name
            FROM
                {$topics_table}
            WHERE
                product_id = %d
            ORDER BY id ASC",
            $this->product_id
        );
        $rows = $wpdb->get_results( $sql );
        if ( count( $rows ) !== 0 ) {
            foreach ( $rows as $row ) {
                array_push( $this->topics, sanitize_text_field( $row->name ) );
            }
        }
        // fetch files meta from database.
        $this->files_meta = [];
        $files_table      = $this->table_prefix . 'files';
        $sql  = $wpdb->prepare( "
            SELECT
                {$files_table}.hash,
                {$files_table}.position,
                {$files_table}.free,
                {$files_table}.attribute,
                {$topics_table}.topic_id AS topic_id
            FROM
                {$files_table} INNER JOIN {$topics_table}
            ON
                {$topics_table}.product_id = %d AND
                {$files_table}.tid = {$topics_table}.id
            ORDER BY topic_id ASC, position ASC",
            $this->product_id
        );
        $rows = $wpdb->get_results( $sql );
        if ( count( $rows ) !== 0 ) {
            foreach ( $rows as $row ) {
                 $this->files_meta[ $row->topic_id ][ $row->position ] = array(
                    'hash'      =>  sanitize_text_field( $row->hash ),
                    'free'      => absint( $row->free ),
                    'attribute' =>  sanitize_text_field( $row->attribute ),
                );
            }
        }
    }

}

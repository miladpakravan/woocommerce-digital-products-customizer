<?php
/**
 * Plugin enqueue class
 *
 * this file for manage enqueue styles and scripts.
 *
 * @package Woodpc
 * @since 1.0.0
 */
namespace WOODPC\Core;
/**
 * a class to manage enqueue styles and scripts.
 *
 * @since 1.0.0
 */
class Enqueue
{
	/**
	 * register Enqueue class.
	 *
	 * register admin/frontend scripts and styles.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function register() {
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'frontend_enqueue' ) );
	}

	/**
	 * enqueue admin scripts and styles
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function admin_enqueue() {
		global $pagenow;
		$screen = get_current_screen();
		// enqueue only if we are in edit product page or plugin settings page.
		if ( ( 'post.php' === $pagenow && 'product' === get_post_type() ) || 'woocommerce_page_woodpc_settings' === $screen->id ) {
			wp_enqueue_style(
				'woodpc-admin-style',
				WOODPC_PLUGIN_URL . 'assets/css/woodpc-admin.css'
			);
			// load rtl mode styles.
			if ( is_rtl() ) {
				wp_enqueue_style(
					'woodpc-admin-style-rtl',
					WOODPC_PLUGIN_URL . 'assets/css/woodpc-admin-rtl.css'
				);
			}
			wp_enqueue_script(
				'woodpc-admin-script',
				WOODPC_PLUGIN_URL . 'assets/js/woodpc-admin.js',
				array( 'jquery' ),
				'',
				true
			);
			/*
			* for localization of remove topic in edit product page,
			* we use this codes.
			*/
			$ajax_data = array(
				'ajax_url' => admin_url( 'admin-ajax.php' ),
				'remove_topic_prompt' => __( 'By removing Topic, related Files will be removed, Are you sure?', 'woodpc' ),
			);
			wp_localize_script(
				'woodpc-admin-script',
				'woodpc_ajax_object',
				$ajax_data
			);
		}
	}

	/**
	 * enqueue frontend scripts and styles.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function frontend_enqueue() {
		// enqueue only if we are in single product page.
		if ( is_singular( array( 'product' ) ) ) {
			wp_enqueue_script(
				'woodpc-frontend-script',
				WOODPC_PLUGIN_URL . 'assets/js/woodpc.js', array( 'jquery-ui-accordion' ) ,
				'',
				true
			);
			wp_enqueue_style(
				'woodpc-frontend-style',
				WOODPC_PLUGIN_URL . 'assets/css/woodpc-frontend.css'
			);
			// load rtl mode styles.
			if ( is_rtl() ) {
				wp_enqueue_style(
					'woodpc-frontend-style-rtl',
					WOODPC_PLUGIN_URL . 'assets/css/woodpc-frontend-rtl.css'
				);
			}
		}
	}
}

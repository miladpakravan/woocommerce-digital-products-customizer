<?php
/**
 * Plugin uninstaller class
 *
 * @package Woodpc
 * @since 1.0.0
 */
namespace WOODPC\Core;
/**
 * Uninstall Plugin class.
 *
 * @since 1.0.0
 */
class Uninstall
{
    /**
     * uninstall plugin and remove its data.
     *
     * @since 1.0.0
     *
     * @global object $wpdb wordpress database handler.
     *
     * @return void
     */
	public static function uninstall() {
        // delete database tables.
        global $wpdb;
        $files_table_name   = $wpdb->prefix . 'woodpc_files';
        $topices_table_name = $wpdb->prefix . 'woodpc_topics';
        $wpdb->query( "DROP TABLE IF EXISTS " . $files_table_name );
        $wpdb->query( "DROP TABLE IF EXISTS " . $topices_table_name );
        // delete wordpress options.
        delete_option( 'woodpc_settings' );
        delete_option( 'woodpc_db_version' );
		flush_rewrite_rules();
	}
}

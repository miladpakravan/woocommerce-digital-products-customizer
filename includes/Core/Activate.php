<?php
/**
 * Plugin activate class
 *
 * this file focus to activate and initialize plugin.
 *
 * @package Woodpc
 * @since 1.0.0
 */
namespace WOODPC\Core;
/**
 * Activate Plugin class.
 *
 * @since 1.0.0
 */
class Activate
{
	/**
	 * activate plugin.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public static function activate() {
		Activate::update_db();
		flush_rewrite_rules();
	}

	/**
	 * create or update plugin database tables.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public static function update_db() {
		global $wpdb;
		// if plugin database version changed during update.
		if ( floatval( get_option( 'woodpc_db_version' ) ) !== WOODPC_DB_VERSION ) {
			/**
			 * for using dbDelta function.
			 */
			require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
			$charset_collate = $wpdb->get_charset_collate();
			// creating topics table.
			$topics_table    = $wpdb->prefix . 'woodpc_topics';
			$sql             = "
				CREATE TABLE IF NOT EXISTS `$topics_table` (
					id int(11) NOT NULL AUTO_INCREMENT,
					topic_id tinyint(4) NOT NULL,
					product_id int(11) NOT NULL,
					name varchar(255) NOT NULL,
					PRIMARY KEY  (id, topic_id, product_id)
				) $charset_collate;";
			dbDelta( $sql );

			// creating files table.
			$files_table = $wpdb->prefix . 'woodpc_files';
			$sql         = "
				CREATE TABLE IF NOT EXISTS `$files_table` (
					id int(11) NOT NULL AUTO_INCREMENT,
					hash varchar(255) NOT NULL,
					tid int(11) NOT NULL,
					position int(11) NOT NULL,
					free tinyint(1) NOT NULL,
					attribute varchar(255) NOT NULL,
					PRIMARY KEY  (id),
					FOREIGN KEY (tid) REFERENCES $topics_table(id) ON DELETE CASCADE
				) $charset_collate;";
			dbDelta( $sql );
			// update database tables version.
			update_option( 'woodpc_db_version', WOODPC_DB_VERSION );
		}
	}

}

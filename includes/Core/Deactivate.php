<?php
/**
 * Plugin deactivate class
 *
 * this file focus to deactivate plugin.
 *
 * @package Woodpc
 * @since 1.0.0
 */
namespace WOODPC\Core;
/**
 * Deactivate Plugin class.
 *
 * @since 1.0.0
 */
class Deactivate
{
	/**
	 * deactivate plugin.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public static function deactivate() {
		flush_rewrite_rules();
	}
}

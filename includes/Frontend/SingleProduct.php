<?php
/**
 * Single Product helper class
 *
 * @package Woodpc
 * @since 1.0.0
 */
namespace WOODPC\Frontend;
use WOODPC\Admin\Admin;
use WOODPC\Core\TemplateLoader;
use WOODPC\Core\DigitalProduct;
/**
 * Plugin Frontend handler class.
 *
 * main functions related to single product page is here.
 *
 * @since 1.0.0
 */
class SingleProduct
{
	/**
	 * register SingleProduct class.
	 *
	 * register plugin shortcode and two action for single product page.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function register() {
		add_shortcode( 'woodpc_files_box', array( $this, 'show_product_files_box' ) );
		add_action( 'woocommerce_single_product_summary', array( $this, 'notice_user_has_already_bought_product' ) );
		add_action('parse_request', array( $this, 'download_free_file' ), 1);
		add_filter( 'woocommerce_checkout_fields' , array( $this, 'filter_woocommerce_checkout_fields' ) , 10, 2 );
		// dynamically add action for set product files box position according plugin settings.
		$this->set_product_files_box_position();
	}

	/**
	 * show product files box template in single product page.
	 *
	 * @since 1.0.0
	 *
	 * @global object $product woocommerce product class object.
	 *
	 * @return void
	 */
	public function show_product_files_box() {
		global $product;
		// if we are in single product page.
		if ( is_singular( array( 'product' ) ) ) {
			// initialize template variables.
			$woodpc_settings  = Admin::get_settings();
			$template_handler = new TemplateLoader;

			// ready downloadable files list
			$download_handler = new DigitalProduct( $product->get_id() );
			$topics           = $download_handler->get_topics();
			/*
			 * if fresh installed plugin, product has not any topics.
			 * so we are consider a Untitled Topic for all files.
			 */
			if ( count( $topics ) === 0 ) {
				$topics = array( __( 'Untitled Topic', 'woodpc' ) );
			}
			$files_meta = $download_handler->get_product_files();
			$data = array(
				'topics'      => $topics,
				'files_meta'  => $files_meta,
			);
			/*
			 * load product files accordion template.
			 * we use Gamajo Template Loader library for support overriden template in theme.
			 */
			ob_start();
			$template_handler->set_template_data( $data, 'woodpc_data' );
			$template_handler->get_template_part( 'single-product', 'single-product' );
			echo ob_get_clean();
		}
	}

	/**
	 * setup product files box position.
	 *
	 * the action will added according to what position selected in plugin settings.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function set_product_files_box_position() {
		$woodpc_settings = Admin::get_settings();
		// if accordion position is enabled.
		if ( 1 === $woodpc_settings['accordion_position_status'] ) {
			$accordion_position    = $woodpc_settings['accordion_position'];
			$woocommerce_positions = array(
				'woocommerce_single_product_summary',
				'woocommerce_after_single_product_summary',
				'woocommerce_after_single_product',
			);
			// add action according to selected accordion position in settings.
			add_action( $woocommerce_positions[ $accordion_position ] , array( $this, 'show_product_files_box' ) );
		}
	}

	/**
	 * fires when customer has already bought current product.
	 *
	 * show a notice to customer in single product page.
	 *
	 * @since 1.0.0
	 *
	 * @global object $product woocommerce product class object.
	 *
	 * @return void
	 */
	public function notice_user_has_already_bought_product() {
		global $product;
		/**
		 * for using woodpc_customer_bought_product function.
		 */
		require_once WOODPC_PLUGIN_DIR . '/libraries/woodpc-helper.php';
		if ( woodpc_customer_bought_product( $product->get_id() ) ) {
			ob_start();
			?>
			<div class="woodpc-product-bought">
				<?php esc_html_e( 'You already bought this product!', 'woodpc' ); ?>
			</div>
			<?php
			echo ob_get_clean();
		}
	}

	/**
	 * fires when user want to download free file.
	 *
	 * this is alternative way for secure download woocommerce product files because of woocommerce can not handle free files.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function download_free_file() {
		// check if query strings and dependencies is available.
		if ( ! ( empty( $_GET['woodpc_hash'] ) || empty( $_GET['p'] ) ) && class_exists('woocommerce') ) {
			$product_id = absint( $_GET['p'] );
			$product    = wc_get_product( $product_id );
			if ( $product ) {
				$file_hash_md5    = sanitize_text_field( $_GET['woodpc_hash'] );
				// get woocommerce product files.
				$woo_downloads    = $product->get_downloads();
				// variable for check file hash exists in product files list.
				$success_download = false;
				foreach ( $woo_downloads as $woo_download ) {
					if ( md5( $woo_download['id'] ) === $file_hash_md5 ) {
						// get file free status from database.
						global $wpdb;
						$files_table = $wpdb->prefix . 'woodpc_files';
						$sql = $wpdb->prepare( "
							SELECT
								free
							FROM
								{$files_table}
							WHERE
								hash = %s",
							$woo_download['id']
						);
						$file_free_status = absint( $wpdb->get_var( $sql ) );
						// if file is free to download.
						if ( 1 === $file_free_status ) {
							/**
							 * for using basic woocommerce download handler methods function ( WC_Download_Handler::download ).
							 * some times woocommmerce plugin installation name changed, so we check that.
							 * if we can not find the class to load, we log error.
							 */
							if ( file_exists( WP_PLUGIN_DIR . '/woocommerce/includes/class-wc-download-handler.php' ) ) {
								require_once WP_PLUGIN_DIR . '/woocommerce/includes/class-wc-download-handler.php';
							} elseif ( file_exists( WP_PLUGIN_DIR . '/WooCommerce/includes/class-wc-download-handler.php' ) ) {
								require_once WP_PLUGIN_DIR . '/WooCommerce/includes/class-wc-download-handler.php';
							} else {
								error_log( 'Woocommerce Digital Product Customizer: can not find woocommerce class-wc-download-hanlder.php' );
								exit();
							}
							/**
							 * for using woodpc_get_file_path_from_download_url function.
							 */
							require_once WOODPC_PLUGIN_DIR . '/libraries/woodpc-helper.php';
							$success_download     = true;
							$file_url             = $woo_download['file'];
							$file_path            = woodpc_get_file_path_from_download_url( $file_url );
							// get current woocommerce file download method settings.
							$file_download_method = apply_filters( 'woocommerce_file_download_method', get_option( 'woocommerce_file_download_method', 'force' ), $product_id, $file_path );
							// if file download method is redirect, send file url instead file_path.
							if ( 'redirect' === $file_download_method ) {
								\WC_Download_Handler::download( $file_url, $product_id );
							} else {
								\WC_Download_Handler::download( $file_path, $product_id );
							}
						}
					}
				}
			}
			// if file not exists in product files list.
			if ( ! $success_download ) {
				/**
				 * for handling download error using woodpc_download_error function.
				 */
				require_once WOODPC_PLUGIN_DIR . '/libraries/woodpc-helper.php';
				woodpc_download_error( __( 'Invalid download link.', 'woodpc' ) );
			}
		}
    }

	/**
	 * filter woocommerce checkout billing fields.
	 *
	 * remove additional checkout fields for virtual products.
	 *
	 * @since 1.0.0
	 */
	public function filter_woocommerce_checkout_fields( $fields ) {
		$only_virtual = true;
		// Check if there are non-virtual products
		foreach( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			if ( ! $cart_item['data']->is_virtual() ) {
				$only_virtual = false;
			}
		}
		// If there are only virtual products in the cart
		if ( $only_virtual ) {
			unset($fields['billing']['billing_company']);
			unset($fields['billing']['billing_address_1']);
			unset($fields['billing']['billing_address_2']);
			unset($fields['billing']['billing_city']);
			unset($fields['billing']['billing_postcode']);
			unset($fields['billing']['billing_country']);
			unset($fields['billing']['billing_state']);
			unset($fields['billing']['billing_phone']);
			add_filter( 'woocommerce_enable_order_notes_field', '__return_false' );
			add_filter( 'woocommerce_cart_needs_shipping', '__return_false' );
			// Set email as first field to help capture cart abandonment
			$fields['billing']['billing_email']['priority'] = -100; // must be first
		}
		return $fields;
	}

}

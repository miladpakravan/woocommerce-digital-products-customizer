<?php
/**
 * Plugin admin section class
 *
 * this file focus plugin admin functionality.
 *
 * @package Woodpc
 * @since 1.0.0
 */
namespace WOODPC\Admin;
use WOODPC\Core\DigitalProduct;
/**
 * Plugin Admin handler class.
 *
 * main admin functions exists in this class.
 *
 * @since 1.0.0
 */
class Admin
{

	/**
	 * register Admin class.
	 *
	 * register actions and filters for admin section.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public function register() {
		// define wp ajax actions with admin permissions for improve security recommended.
		if ( is_admin() ) {
			add_action( 'wp_ajax_woodpc_generate_hash', array( $this, 'generate_file_hash' ) );
		}
		/**
		 * to use current_user_can function in this section.
		 */
		require_once( ABSPATH . 'wp-includes/capabilities.php' );
		if ( current_user_can( 'manage_woocommerce' ) ) {
			add_action( 'admin_menu', array( $this, 'add_admin_pages' ) );
			add_action( 'admin_post_woodpc_settings_form', array( $this, 'save_settings' ) );
			add_action( 'add_meta_boxes', array( $this, 'product_files_metabox' ) );
			add_action( 'save_post', array( $this, 'regenerate_product_orders_downloads_permissions' ), 10, 2 );
			add_filter( 'postbox_classes_product_woodpc-metabox', array( $this, 'product_files_metabox_classes' ) );
			add_filter( 'wp_insert_post_data' , array( $this, 'attemp_to_store_product_meta_data' ), 1, 2 );
		}
	}

	/**
	 * fires when plugin activated and add plugin settings menu.
	 *
	 * @since 1.0.0
	 */
	public function add_admin_pages() {
		add_submenu_page(
			'woocommerce',
			'Woocommerce Digital Products Customizer',
			__( 'Digital Products Customizer', 'woodpc' ),
			'manage_woocommerce', 'woodpc_settings',
			array( $this, 'settings_page' )
		);
	}

	/**
	 * show plugin settings page.
	 *
	 * @since 1.0.0
	 *
	 * @global string WOODPC_PLUGIN_DIR
	 *
	 * @return void
	 */
	public function settings_page() {
		$woodpc_settings = $this->get_settings();
		/**
		 * load Plugin settings template.
		 */
		include WOODPC_PLUGIN_DIR . '/partials/admin/settings/settings.php';
	}

	/**
	 * fires when saving plugin settings.
	 *
	 * get and sanitize posted plugin settings fields and then save settings in database.
	 *
	 * @since 1.0.0
	 */
	public function save_settings() {
		// check if form posted securely.
		if ( current_user_can( 'manage_woocommerce' ) && isset( $_POST['_wpnonce'] ) && wp_verify_nonce( sanitize_text_field( $_POST['_wpnonce'] ), 'woodpc_settings_form' ) ) {
			// if user click on Default button
			if ( isset( $_POST['reset'] ) ) {
				delete_option( 'woodpc_settings' );
			} else {
				$settings = array(
					'disable_auto_update_orders' => isset( $_POST[ 'disable_auto_update_orders' ] ) ? 1 : 0,
					'repurchase'                 => isset( $_POST[ 'repurchase' ] ) ? 1 : 0,
					'accordion_position_status'  => isset( $_POST[ 'accordion_position_status' ] ) ? 1 : 0,
					'accordion_position'         => isset( $_POST[ 'accordion_position' ] ) ? absint( $_POST[ 'accordion_position' ] ) : 0,
				);
				update_option( 'woodpc_settings', $settings );
			}
			$redirect_url = add_query_arg(
				'page',
				'woodpc_settings',
				get_admin_url() . 'admin.php'
			);
			wp_safe_redirect( $redirect_url );
			exit();
		} else {
			$queries = array(
				'page'  => 'woodpc_settings',
				'error' => 'unauthenticated',
			);
			$redirect_url = add_query_arg( $queries, get_admin_url() . 'admin.php' );
			wp_safe_redirect( $redirect_url );
			exit();
		}
	}

	/**
	 * get plugin settings from database.
	 *
	 * if plugin installed recently and settings not exists, return default settings.
	 *
	 * @since 1.0.0
	 *
	 * @return array settings fields.
	 */
	public static function get_settings() {
		$settings = get_option( 'woodpc_settings' );
		if ( $settings ) {
			foreach ( $settings as $key => $value ) {
				// sanitize settings data.
				$settings[ $key ] = absint( $value );
			}
		} else {	// if settings not exists load default settings ( clean install ).
			$settings = array(
				'disable_auto_update_orders' => 0,
				'repurchase'                 => 0,
				'accordion_position_status'  => 0,
				'accordion_position'         => 0,
 		 	);
		}
		return $settings;
	}

	/**
	 * fires when we go to the product edit page.
	 *
	 * add new metabox for better manage product files.
	 *
	 * @since 1.0.0
	 */
	public function product_files_metabox( $post_type ) {
		global $post;
    	$product = wc_get_product( $post->ID );
		// load metabox only if post type is product and product type is simple.
		if ( 'product' === $post_type && $product->is_type( 'simple' ) ) {
			add_meta_box(
				'woodpc-metabox',
				__( 'Product files', 'woodpc' ),
				array( $this, 'product_options_downloads' ),
				'product',
				'normal',
				'default'
			);
		}
    }

	/**
	 * filter product files metabox classes.
	 *
	 * add `woodpc` class to product files metabox.
	 *
	 * @since 1.0.0
	 *
	 * @param array $classes The current classes on the metabox.
	 */
   	public function product_files_metabox_classes( $classes = array() ) {
		global $post;
    	$product = wc_get_product( $post->ID );
		// load metabox only if post type is product and product type is simple.
		if ( 'product' === $post->post_type && $product->is_type( 'simple' ) ) {
			$add_classes = array( 'woodpc' );
			foreach ( $add_classes as $class ) {
				if ( ! in_array( $class, $classes ) ) {
					$classes[] = sanitize_html_class( $class );
				}
			}
		}
	   return $classes;
  	}

	// TODO: fix function docblock
	/**
	 * fires when the product is being published or updated.
	 *
	 * @since 1.0.0
	 */
	public function attemp_to_store_product_meta_data( $data , $postarr ) {
		// sanitize post id as product id.
		$product_id = absint( $postarr['ID'] );
    	$product = wc_get_product( $product_id );
		// load metabox only if post type is product and product type is simple.
		if ( 'product' === $postarr['post_type'] && $product->is_type( 'simple' ) ) {
			// sanitize posted meta box data.
			$sanitized_meta_data = $this->sanitize_product_meta_data( $postarr );
			if ( is_array( $sanitized_meta_data ) ) {
				/*
				 * store product files attribute, access type (files meta data can be access by file hash).
				 * file name, file hash, file url will be store with basic woocommerce functions.
				 */
				$digital_product_handler = new DigitalProduct( $product_id );
				$digital_product_handler->store_product_meta_data( $sanitized_meta_data );
			}
			/*
			 * remove posted file attributes and file access types because we stored them.
			 * we remove them for prevent conflict with woocommerce and other plugins.
			 */
			unset( $postarr['_wc_file_attributes'] );
			unset( $postarr['_wc_file_access_types'] );
		}
		return $data;
	}

	/**
	 * fires when click on add file button in product files metabox.
	 *
	 * this function generate new file hash for compatibility with woocommerce downloads structure.
	 *
	 * @since 1.0.0
	 */
	public function generate_file_hash() {
		// simulating woocommerce file hash generator for product files.
		$response = array(
			'status' => 'success',
			'hash'   => wp_generate_uuid4(),
		);
		wp_send_json( $response );
	}

	/**
	 * show product files metabox template.
	 *
	 * show saved product topics and files meta.
	 *
	 * @since 1.0.0
	 *
	 * @global object $product_object woocommerce product object.
	 *
	 * @return void
	 */
	public function product_options_downloads() {
		global $product_object;
		// get stored product meta data.
		$digital_product_handler = new DigitalProduct( $product_object->get_id() );
		// get stored product topics.
		$topics = $digital_product_handler->get_topics();
		// consider fresh installed plugin and product's topics not set yet.
		if ( 0 === count( $topics ) ) {
			$topics = array( '' );
		}
		/*
		 * add original woocommerce files download meta data that contains:
		 * file name as name, file hash as id, file url as file
		 */
		$files_meta = $digital_product_handler->get_files_meta();
		$downloads = $product_object->get_downloads( 'edit' );
		$downloads_list = [];
		$downloads_list_index = 0;
		foreach ( $downloads as $download ) {
			$meta_data = array(
				'name' => $download['data']['name'],
				'file' => $download['data']['file'],
				'hash' => $download['data']['id'],
			);
			array_push( $downloads_list, $meta_data );
		}
		// consider fresh installed plugin and product's topics not set yet.
		if ( count( $files_meta ) !== 0 ) {
			for ( $topic_id = 0; $topic_id < count( $files_meta ); $topic_id++ ) {
				for ( $position = 0; $position < count( $files_meta[ $topic_id ] ); $position++ ) {
					$files_meta[ $topic_id ][ $position ] = array(
						'hash'      => $files_meta[ $topic_id ][ $position ]['hash'],
						'free'      => $files_meta[ $topic_id ][ $position ]['free'],
						'attribute' => $files_meta[ $topic_id ][ $position ]['attribute'],
						'name'      => $downloads_list[ $downloads_list_index ]['name'],
						'url'       => $downloads_list[ $downloads_list_index ]['file'],
					);
					$downloads_list_index++;
				}
			}
		} else {
			for ( $position = 0; $position < count( $downloads_list ); $position++ ) {
				$files_meta[0][ $position ] = array(
					'hash'      => $downloads_list[ $downloads_list_index ]['hash'],
					'free'      => 0,
					'attribute' => "",
					'name'      => $downloads_list[ $downloads_list_index ]['name'],
					'url'       => $downloads_list[ $downloads_list_index ]['file'],
				);
				$downloads_list_index++;
			}
		}
		/**
		 * load product files metabox template.
		 */
		include WOODPC_PLUGIN_DIR . '/partials/admin/product/meta-box.php';
	}

	/**
	 * fires when publish/update post.
	 *
	 * auto regenerate downloads permissions for product orders.
	 *
	 * @since 1.0.0
	 *
	 * @global object $wpdb wordpress database handler.
	 *
	 * @param int $post_id published/updated wordpress post id.
	 * @param object $post published/updated wordpress post data.
	 *
	 * @return void
	 */
	public function regenerate_product_orders_downloads_permissions( $post_id, $post ) {
		global $wpdb;
		// load metabox only if post type is product and product type is simple.
		if ( 'product' === $post->post_type && class_exists( 'WooCommerce' ) ) {
			$woodpc_settings = $this->get_settings();
			$disable_auto_update_orders = $woodpc_settings['disable_auto_update_orders'];
			// if auto update product orders is enabled.
			if ( 0 === $disable_auto_update_orders ) {
				$product_object = wc_get_product( absint( $post_id ) );
				// if product type is digital and simple.
				if ( $product_object->is_downloadable( 'yes' ) && $product_object->is_type( 'simple' ) ) {
					// get all WooCommerce complete orders id by product id.

					// Define HERE the orders status to include in  <==  <==  <==  <==  <==  <==  <==
					$orders_statuses = 'wc-completed';

					// Get All defined statuses Orders IDs for a defined product ID (or variation ID).
					$prepared_sql = $wpdb->prepare( "
						SELECT 
							DISTINCT woi.order_id
						FROM
							{$wpdb->prefix}woocommerce_order_itemmeta as woim,
							{$wpdb->prefix}woocommerce_order_items as woi,
							{$wpdb->prefix}posts as p
						WHERE
							woi.order_item_id = woim.order_item_id AND
							woi.order_id = p.ID AND
							p.post_status IN ( %s ) AND
							woim.meta_key IN ( '_product_id', '_variation_id' ) AND
							woim.meta_value = '%d'
						ORDER BY woi.order_item_id DESC",
						$orders_statuses,
						$post_id
					);
					$orders_id = $wpdb->get_col( $prepared_sql );

					foreach ($orders_id as $order_id) {
						// populate order billing email if not set.
						$order      = wc_get_order( $order_id );
						$order_data = $order->get_data(); // The Order data
						if ( is_email( $order_data['billing']['email'] ) ) {
							$order_billing_email = $order_data['billing']['email'];
						} else {
							$user                = $order->get_user(); // Get the WP_User object
							$user_info = $this->get_userdata( $user->ID );
							$order_billing_email = $user_info->user_email;
						}
						$order_info = array (
							'order_id' => $order_id,
							'email'    => $order_billing_email,
						);
						$order = wc_update_order( $order_info );

						/*
						 * Remove all existing download permissions for this order.
						 * This uses the same code as the "regenerate download permissions" action in the WP admin (https://github.com/woocommerce/woocommerce/blob/3.5.2/includes/admin/meta-boxes/class-wc-meta-box-order-actions.php#L129-L131)
						 * An instance of the download's Data Store (WC_Customer_Download_Data_Store) is created and
						 * uses its method to delete a download permission from the database by order ID.
						 */
						$data_store = \WC_Data_Store::load( 'customer-download' );
						$data_store->delete_by_order_id( $order_id );

						/* Run WooCommerce's built in function to create the permissions for an order (https://docs.woocommerce.com/wc-apidocs/function-wc_downloadable_product_permissions.html)
						 * Setting the second "force" argument to true makes sure that this ignores the fact that permissions
						 * have already been generated on the order.
						 */
						wc_downloadable_product_permissions( $order_id, true );
					}
				}
			}
		}
	}

	// TODO: fix function docblock
	/**
	 * sanitize product topics and files meta sent by wp-ajax.
	 *
	 * data sent by wp-ajax needs sanitized.
	 *
	 * @since 1.0.0
	 *
	 * @param array $meta_data {
     *     @type string $topic_name the topic name
     *     @type array $files_meta {
     *         @type string $hash woocommerce file hash (file id)
     *         @type string $attribute file attribute
     *         @type int $free if file is free to download, free equal 1
     *     }
	 * }
	 *
	 * @return array|null if meta data is not array return null.
	 */
	private function sanitize_product_meta_data( $postarr ) {
		// check if topics and files posted.
		if ( isset( $postarr['_wc_topics'], $postarr['_wc_file_hashes'] ) ) {
			// get file names, file hashes, file urls, topics posted data.
			$names  = $postarr['_wc_file_names'];
			$hashes = $postarr['_wc_file_hashes'];
			$urls   = $postarr['_wc_file_urls'];
			$topics = $postarr['_wc_topics'];

			/*
			 * rebase attributes and access types array keys.
			 * when a topic removed, rebase needed.
			 */
			$postarr['_wc_file_attributes'] = array_merge( $postarr['_wc_file_attributes'] );
			$postarr['_wc_file_access_types'] = array_merge( $postarr['_wc_file_access_types'] );

			// sanitized array for meta data.
			$sanitized_meta_data = [];
			// two index for woocommerce files meta and topic.
			$file_index = 0;
			$topic_id = 0;
			for ( $topic_index = 0; $topic_index < count( $topics ); $topic_index++ ) {
				// when topic has not any files, we skip that topic.
				if ( ! ( isset( $postarr['_wc_file_attributes'][ $topic_index ] ) && isset( $postarr['_wc_file_access_types'][ $topic_index ] ) ) ) {
					continue;
				}
				/*
				 * for ease of management of files meta, file attributes and access types orderd by topic id.
				 * so we can get file attributes and access types for every topic easily.
				 */
				$attributes = $postarr['_wc_file_attributes'][ $topic_index ];
				$access_types = $postarr['_wc_file_access_types'][ $topic_index ];
				// index for manage meta data position.
				$position = 0;
				$sanitized_meta_data_part = [];
				for ( $meta_index = 0; $meta_index < count( $attributes ); $meta_index++ ) {
					// we store only files that has file name and file url.
					if ( ! ( empty( $names[ $file_index ] ) || empty( $urls[ $file_index ] ) ) ) {
						$sanitized_meta_data_part[ $position ] = array(
							'hash'      => sanitize_text_field( $hashes[ $file_index ] ),
							'attribute' => sanitize_text_field( $attributes[ $meta_index ] ),
							'free'      => absint( $access_types[ $meta_index ] ),
						);
						$position++;
					}
					$file_index++;
				}
				// if topic has a file at least, we will be add this topic.
				if ( 0 < count( $sanitized_meta_data_part ) ) {
					/*
						* sanitize and add new topic.
						* consider a files meta array for new topic.
						*/
					$sanitized_meta_data[ $topic_id ] = array(
						'topic_name' => sanitize_text_field( $topics[ $topic_index ] ),
						'files_meta' => $sanitized_meta_data_part,
					);
					$topic_id++;
				}
			}
			return $sanitized_meta_data;
		}
		return null;
	}

}

<?php
/**
 * Plugin Initializer class
 *
 * @package Woodpc
 * @since 1.0.0
 */
namespace WOODPC;
/**
 * Initialize Plugin classes.
 *
 * @since 1.0.0
 */
final class Init
{
	/**
	 * store the classes inside an array.
	 *
	 * depends on WooCommerce plugin is activated or not.
	 *
	 * @since 1.0.0
	 *
	 * @return array list of classes.
	 */
	public static function get_services() {
		// if woocommerce plugin is activated, all classes will be loaded.
		if ( class_exists( 'WooCommerce' ) ) {
			return [
				Core\DependencyChecker::class,
				Core\SettingsLinks::class,
				Core\Enqueue::class,
				Admin\Admin::class,
				Frontend\SingleProduct::class,
			];
		} else {
			return [
				Core\DependencyChecker::class,
				Core\SettingsLinks::class,
			];
		}
	}

	/**
	 * loop through the classes, initialize them and call the register() method if it exists.
	 *
	 * @since 1.0.0
	 *
	 * @return void
	 */
	public static function register_services() {
		foreach ( self::get_services() as $class ) {
			$service = self::instantiate( $class );
			if ( method_exists( $service, 'register' ) ) {
				$service->register();
			}
		}
	}

	/**
	 * initialize the class.
	 *
	 * @since 1.0.0
	 *
	 * @param  class $class    class from the services array.
	 *
	 * @return class instance  new instance of the class.
	 */
	private static function instantiate( $class ) {
		$service = new $class();
		return $service;
	}
}

# Plugin Description

This Plugin will improve Woocommerce Digital Products User Experience.

## Shortcode

Shortcode to show product downloadable files in single product template:

```bash
[woodpc_files_box]
```

## Template Overriden

You can override product accordion box by creating 'woocommerce-digital-products-customizer' directory in your theme and then copy files from templates directory to it.

Support Email: info@shetaweb.com

Release Version: 1.0.0

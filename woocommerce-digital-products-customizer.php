<?php
/**
 * Plugin manager file
 *
 * @package Woodpc
 * @since 1.0.0
 */

/*
* Plugin Name: Woocommerce Digital Products Customizer
* Plugin URI: https://shetaweb.com/plugins
* Description: Better UX for Woocommerce Digital Products
* Version: 1.0.0
* Author: Shetaweb Team
* Author URI: https://shetaweb.com
* Text Domain: woodpc
* Domain Path: /languages
*/

// If this file is called firectly, abort!!!
if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not authorized to view this page!' );
}

/**
 * Refer to Plugin Directory
 *
 * @since 1.0.0
 * @var string WOODPC_PLUGIN_DIR
 */
define( 'WOODPC_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
/**
 * Refer to Plugin URL
 *
 * @since 1.0.0
 * @var string WOODPC_PLUGIN_URL
 */
define( 'WOODPC_PLUGIN_URL', plugin_dir_url( __FILE__ ) );
/**
 * Refer to Plugin Name
 *
 * @since 1.0.0
 * @var string WOODPC_PLUGIN_NAME
 */
define( 'WOODPC_PLUGIN_NAME', plugin_basename( __FILE__ ) );
/**
 * Refer to Plugin database version
 *
 * @since 1.0.0
 * @var float WOODPC_DB_VERSION
 */
define( 'WOODPC_DB_VERSION', 1.3 );

if ( file_exists( WOODPC_PLUGIN_DIR . '/vendor/autoload.php' ) ) {
	/**
	 * load Vendor autoload.
	 */
	require_once WOODPC_PLUGIN_DIR . '/vendor/autoload.php';
}

/**
 * fires when plugin loaded.
 *
 * load plugin text domain for localization.
 *
 * @since 1.0.0
 *
 * @return void
 */
function woodpc_load_textdomain_plugin() {
	load_plugin_textdomain( 'woodpc', false, dirname( plugin_basename( __FILE__ ) ) . '/languages'  );
}
add_action( 'plugins_loaded', 'woodpc_load_textdomain_plugin' );


/**
 * The code that runs during plugin activation.
 *
 * @since 1.0.0
 *
 * @return void
 */
function woodpc_activate_plugin() {
	if ( class_exists( 'WOODPC\Core\Activate' ) ) {
		WOODPC\Core\Activate::activate();
	}
}

/**
 * The code that runs during plugin deactivation.
 *
 * @since 1.0.0
 *
 * @return void
 */
function woodpc_deactivate_plugin() {
	if ( class_exists( 'WOODPC\Core\Deactivate' ) ) {
		WOODPC\Core\Deactivate::deactivate();
	}
}

/**
 * The code that runs during plugin uninstallation.
 *
 * @since 1.0.0
 *
 * @return void
 */
function woodpc_uninstall_plugin() {
	if ( class_exists( 'WOODPC\Core\Uninstall' ) ) {
		WOODPC\Core\Uninstall::uninstall();
	}
}

/**
 * Initialize all the core classes of the plugin.
 *
 * @since 1.0.0
 *
 * @return void
 */
function woodpc_init() {
	if ( class_exists( 'WOODPC\\Init' ) ) {
		WOODPC\Init::register_services();
	}
}
add_action( 'plugins_loaded', 'woodpc_init' );

register_activation_hook( __FILE__, 'woodpc_activate_plugin' );
register_deactivation_hook( __FILE__, 'woodpc_deactivate_plugin' );
register_uninstall_hook( __FILE__, 'woodpc_uninstall_plugin' );

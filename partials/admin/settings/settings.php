<?php
/**
 * Plugin settings template
 *
 * @package Woodpc
 * @since 1.0.0
 */
?>
<div class="woodpc-settings">
  <h1><?php esc_html_e( 'Woocommerce Digital Products Customizer', 'woodpc'); ?></h1>
  <?php if ( ! ( defined( 'ABSPATH' ) && current_user_can( 'manage_woocommerce' ) ) ): ?>
    <p><?php esc_html_e( 'You are not authorized to perform this operation.', 'woodpc' ); ?></p>
    <?php die(); ?>
  <?php endif; ?>
  <?php if ( ! isset( $woodpc_settings ) ) : ?>
    <p><?php esc_html_e( 'There is a problem to get plugin settings, Please Uninstall and Reinstall Plugin or Contact Support', 'woodpc' ); ?></p>
    <?php die(); ?>
  <?php endif; ?>
  <?php $woodpc_settings_nonce = wp_create_nonce( 'woodpc_settings_form_nonce' ); ?>
    <form action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" method="post">
      <div class="auto-update-orders">
        <input type="checkbox" id="disable-auto-update-orders" name="disable_auto_update_orders" value="disable_auto_update_orders" <?php if ( 1 === $woodpc_settings['disable_auto_update_orders'] ) echo 'checked'; ?> />
        <label for="disable-auto-update-orders"><?php esc_html_e( 'disable auto update orders', 'woodpc' ); ?></label>
        <p><?php esc_html_e( 'by enabling this option you will prevent auto regenerate downloads permissions for digital product orders while add or update product.', 'woodpc' ); ?></p>
        <p class="important-note"><?php esc_html_e( 'Caution: Customers who have already purchased digital products will not be able to access the new topics and files', 'woodpc' ); ?></p>
      </div>
      <div class="repurchase">
        <input type="checkbox" id="repurchase" name="repurchase" value="repurchase" <?php if ( 1 === $woodpc_settings['repurchase'] ) echo 'checked'; ?> />
        <label for="repurchase"><?php esc_html_e( 'repurchasable', 'woodpc' ); ?></label>
        <p><?php esc_html_e( 'by enabling this option user can repurchase digital products', 'woodpc' ); ?></p>
      </div>
      <div>
        <div class="set-accordion-position">
          <input type="checkbox" id="accordion-position-status" name="accordion_position_status" value="<?php echo esc_attr( $woodpc_settings['accordion_position_status'] ); ?>" <?php if ( 1 === $woodpc_settings['accordion_position_status'] ) echo 'checked'; ?> />
          <label for="accordion-position-status"><?php esc_html_e( 'enable accordion position', 'woodpc' ); ?></label>
          <p><?php esc_html_e( 'by enabling this option you can setup accordion position in woocommerce product page', 'woodpc' ); ?></p>
        </div>
        <div class="accordion-position">
          <label for="accordion-position"><?php esc_html_e( 'accordion position', 'woodpc' ); ?></label>
          <select name="accordion_position" id="accordion-position" <?php if ( 0 === $woodpc_settings['accordion_position_status'] ) echo 'disabled'; ?>>
            <option value="0" <?php if ( 0 === $woodpc_settings['accordion_position'] ) echo 'selected="selected"'; ?>><?php esc_html_e( 'before add to cart', 'woodpc' ); ?></option>
            <option value="1" <?php if ( 1 === $woodpc_settings['accordion_position'] ) echo 'selected="selected"'; ?>><?php esc_html_e( 'after product summary', 'woodpc' ); ?></option>
            <option value="2" <?php if ( 2 === $woodpc_settings['accordion_position'] ) echo 'selected="selected"'; ?>><?php esc_html_e( 'at end of product section', 'woodpc' ); ?></option>
          </select>
        </div>
        <div class="shortcode">
          <p><?php esc_html_e( 'if you prefer manually setup accordion position, you can use below shortcode', 'woodpc' ); ?></p>
          <code>[woodpc_files_box]</code>
        </div>
      </div>
      <div>
        <?php wp_nonce_field( 'woodpc_settings_form'); ?>
        <input type="hidden" name="action" value="woodpc_settings_form" />
        <input type="submit" name="submit" id="submit" class="button button-primary" value="<?php esc_attr_e( 'Save Changes', 'woodpc' ); ?>" />
        <input type="submit" name="reset" id="reset" class="button button-primary" value="<?php esc_attr_e( 'Default', 'woodpc' ); ?>" />
      </div>
    </form>
</div>
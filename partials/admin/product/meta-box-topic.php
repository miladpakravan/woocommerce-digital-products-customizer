<?php
/**
 * Product metabox topic template
 *
 * @package Woodpc
 * @since 1.0.0
 */
?>
<?php if ( ! ( defined( 'ABSPATH' ) && current_user_can( 'manage_woocommerce' ) ) ): ?>
    <p><?php esc_html_e( 'You are not authorized to view downloadable files', 'woodpc' ); ?></p>
    <?php die(); ?>
<?php endif; ?>
<?php $topic_id   = isset( $topic['id'] ) ? absint( $topic['id'] ) : 0; ?>
<?php $topic_name = isset( $topic['name'] ) ? $topic['name'] : ''; ?>
<div class="topic-box">
    <div class="topic-header">
        <span class="topic-label"><?php esc_html_e( 'Topic:', 'woodpc' ); ?></span>
        <input type="text" class="topic-name" data-topic-id="<?php echo esc_attr( $topic_id );?>" name="_wc_topics[]" value="<?php echo esc_attr( $topic_name );?>"/>
        <a class="delete-topic"><?php esc_html_e( 'Remove', 'woodpc' ); ?></a>
    </div>
    <table>
        <thead>
            <tr>
                <th></th>
                <th><?php esc_html_e( 'Title', 'woodpc' ); ?></th>
                <th><?php esc_html_e( 'URL', 'woodpc' ); ?></th>
                <th><?php esc_html_e( 'Browse', 'woodpc' ); ?></th>
                <th><?php esc_html_e( 'Attribute', 'woodpc' ); ?></th>
                <th><?php esc_html_e( 'Free', 'woodpc' ); ?></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?php
            if ( isset( $files_meta[ $topic_id ] ) && is_array( $files_meta[ $topic_id ] ) && count( $files_meta[ $topic_id ] ) !== 0 ) {
                for ( $position = 0; $position < count( $files_meta[ $topic_id ] ); $position++ ) {
                    ob_start();
                    $file_meta = $files_meta[ $topic_id ][ $position ];
                    /**
                     * load product files metabox table row template.
                     */
                    include WOODPC_PLUGIN_DIR . '/partials/admin/product/meta-box-file.php';
                    echo ob_get_clean();
                }
            } else {
                ob_start();
                $position = 0;
                /**
                 * load product files metabox table row template.
                 */
                include WOODPC_PLUGIN_DIR . '/partials/admin/product/meta-box-file.php';
                echo ob_get_clean();
            }
        ?>
        </tbody>
    </table>
    <div class="new-file">
        <a class="add-file-button" data-row='
            <?php
                // generate new file data-row value
                ob_start();
                // empty file_meta before add new file.
                $file_meta = array(
                    'hash'      => '',
                    'attribute' => '',
                    'free'      => 0,
                    'url'       => '',
                    'name'      => '',
                );
                /**
                 * load product files metabox table row template.
                 */
                include WOODPC_PLUGIN_DIR . '/partials/admin/product/meta-box-file.php';
                echo esc_attr( ob_get_clean() );
            ?>
        '><?php esc_html_e( 'Add File', 'woodpc' ); ?></a>
    </div>
</div>

<?php
/**
 * Product metabox template
 *
 * @package Woodpc
 * @since 1.0.0
 */
?>
<?php if ( ! ( defined( 'ABSPATH' ) && current_user_can( 'manage_woocommerce' ) ) ): ?>
    <p> <?php esc_html_e( 'You are not authorized to view product files', 'woodpc' ); ?> </p>
    <?php die(); ?>
<?php endif; ?>
<?php if ( ! isset( $topics, $files_meta ) ) : ?>
	<p><?php esc_html_e( 'Error occures while get product files', 'woodpc' ); ?></p>
	<?php die(); ?>
<?php endif; ?>
<?php
// load product topic box templates.
for ( $topic_id = 0; $topic_id < count( $topics ); $topic_id++ ) {
    $topic = array(
        'id'   => $topic_id,
        'name' => $topics[ $topic_id ],
    );
    /**
     * load product files metabox topic template.
     */
    include WOODPC_PLUGIN_DIR . '/partials/admin/product/meta-box-topic.php';
}
?>
<div class="new-topic">
    <a class="add-topic-button" data-row='
        <?php
            // generate new topic data-row value.
            ob_start();
            $topic = array(
                'id'   => count( $topics ),
                'name' => '',
            );
            // empty files_meta before add new topic.
            $files_meta = null;
            /**
             * load product files metabox topic template.
             */
            include WOODPC_PLUGIN_DIR . '/partials/admin/product/meta-box-topic.php';
            echo esc_attr( ob_get_clean() );
        ?>
    '><?php esc_html_e( 'Add topic', 'woodpc' ); ?></a>
</div>

<?php
/**
 * Product metabox file template
 *
 * @package Woodpc
 * @since 1.0.0
 */
?>
<?php if ( ! ( defined( 'ABSPATH' ) &&  current_user_can( 'manage_woocommerce' ) ) ): ?>
	<p><?php _e( 'You are not authorized to view downloadable files', 'woodpc' ); ?></p>
	<?php die(); ?>
<?php endif; ?>
<?php if ( ! isset( $file_meta ) ) : ?>
	<?php
	$file_meta = array(
		'hash'      => '',
		'attribute' => '',
		'free'      => 0,
		'url'       => '',
		'name'      => '',
	);
	?>
<?php endif; ?>
<?php if ( ! isset( $topic_id ) ) : ?>
	<?php $topic_id = 0; ?>
<?php endif; ?>
<?php if ( ! isset( $position ) ) : ?>
	<?php $position = 0; ?>
<?php endif; ?>
<tr>
	<td class="sort"></td>
	<td data-label="<?php esc_attr_e( 'Title', 'woodpc' ); ?>">
		<input type="text" class="file-name" placeholder="<?php esc_attr_e( 'File name', 'woodpc' ); ?>" name="_wc_file_names[]" value="<?php echo esc_attr( $file_meta['name'] ); ?>" />
		<input type="hidden" class="file-hash" name="_wc_file_hashes[]" value="<?php echo esc_attr( $file_meta['hash'] ); ?>" />
	</td>
	<td data-label="<?php esc_attr_e( 'URL', 'woodpc' ); ?>">
		<input type="text" class="file-url" placeholder="<?php esc_attr_e( 'http://', 'woodpc' ); ?>" name="_wc_file_urls[]" value="<?php echo esc_url( $file_meta['url'] ); ?>" />
	</td>
	<td class="upload-file" data-label="<?php esc_attr_e( 'Browse', 'woodpc' ); ?>">
		<a class="upload-file-button" data-choose="<?php esc_attr_e( 'Choose file', 'woodpc' ); ?>" data-update="<?php esc_attr_e( 'Insert file URL', 'woodpc' ); ?>">...</a>
	</td>
	<td data-label="<?php esc_attr_e( 'Attribute', 'woodpc' ); ?>">
		<input type="text" class="file-attribute" name="_wc_file_attributes[<?php echo esc_attr( $topic_id ); ?>][]" maxlength="10" value="<?php echo esc_attr( $file_meta['attribute'] ); ?>" />
	</td>
	<td class="free-status" data-label="<?php esc_attr_e( 'Free', 'woodpc' ); ?>">
		<input type="checkbox" class="file-access-type" name="_wc_file_access_types[<?php echo esc_attr( $topic_id ); ?>][]" value="1"  <?php if ( isset( $file_meta['free'] ) && 1 === absint( $file_meta['free'] ) ) echo 'checked'; ?> />
		<?php if ( ! isset( $file_meta['free'] ) || 1 !== absint( $file_meta['free'] ) ) : ?>
			<input type="hidden" class="file-access-type" name="_wc_file_access_types[<?php echo esc_attr( $topic_id ); ?>][]" value="0" checked />
		<?php endif; ?>
	</td>
	<td>
		<a class="delete-file"><?php esc_html_e( 'Remove File', 'woodpc' ); ?></a>
	</td>
</tr>

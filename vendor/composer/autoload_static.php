<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite805c7f54ad6e08da10fb6883c93cbd9
{
    public static $prefixLengthsPsr4 = array (
        'W' => 
        array (
            'WOODPC\\' => 7,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'WOODPC\\' => 
        array (
            0 => __DIR__ . '/../..' . '/includes',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInite805c7f54ad6e08da10fb6883c93cbd9::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInite805c7f54ad6e08da10fb6883c93cbd9::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInite805c7f54ad6e08da10fb6883c93cbd9::$classMap;

        }, null, ClassLoader::class);
    }
}

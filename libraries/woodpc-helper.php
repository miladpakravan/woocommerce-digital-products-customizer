<?php
/**
 * Woocommerce helpers functions
 *
 * @package Woodpc
 * @since 1.0.0
 */

/**
 * check customer bought product.
 *
 * @since 1.0.0
 *
 * @param int $product_id woocommerce product id.
 *
 * @return bool
 */
function woodpc_customer_bought_product( $product_id ) {
    if ( is_user_logged_in() && function_exists( 'wc_customer_bought_product' ) ) {
        $current_user = wp_get_current_user();
        /*
         * check customer bought product with woocommerce function.
         * function location: woocommerce/includes/wc-user-functions.php
         */
        return wc_customer_bought_product( $current_user->user_email, $current_user->ID, $product_id );
    }
    return false;
}

/**
 * get product file path from downloadable url
 *
 * @since 1.0.0
 *
 * @param string $file_url downloadable file url
 *
 * @return string file path/file url.
 */
function woodpc_get_file_path_from_download_url( $file_url ) {
    // is url internal?
    if ( preg_match( site_url(), $file_url, $match ) ) {
        preg_match( '/\/uploads\/(.*)/', $file_url, $file_path_partial );
        $upload_dir = wp_get_upload_dir();
        $file_path  = $upload_dir['basedir'] . '/' . $file_path_partial[1];
        return $file_path;
    }
    return $file_url;
}

/**
 * get downloadable url for free file.
 *
 * given that we want to secure free files url too, this function can help us.
 *
 * @since 1.0.0
 *
 * @param string $product_id woocommerce product id.
 * @param string $hash woocommerce product file hash.
 *
 * @return string secure downloadable url.
 */
function woodpc_get_free_file_downloadable_url( $product_id, $hash ) {
    // hide file hash using md5 for better security.
    $queries = array(
        'p'           => $product_id,
        'woodpc_hash' => md5( $hash ),
    );
    // return url with anatomy => <domain.tld>/?p=`<product_id>&woodpc_hash=<hash>
    return add_query_arg( $queries, get_permalink( $product_id ) );
}

/**
 * Die with an error message if the download fails.
 *
 * This uses the same code as the "download_error" function in the WooCommerce.
 * @see WC_Download_Handler:download_error
 *
 * @param string  $message Error message.
 * @param string  $title   Error title.
 * @param integer $status  Error status.
 *
 * @return void
 */
function woodpc_download_error( $product_id ) {
    /*
     * Since we will now render a message instead of serving a download, we should unwind some of the previously set
     * headers.
     */
    header( 'Content-Type: ' . get_option( 'html_type' ) . '; charset=' . get_option( 'blog_charset' ) );
    header_remove( 'Content-Description;' );
    header_remove( 'Content-Disposition' );
    header_remove( 'Content-Transfer-Encoding' );

    $title = 'woodpc';
    $message .= ' <a href="' . esc_url( get_permalink( $product_id ) ) . '" class="wc-forward">' . esc_html__( 'Invalid download link.', 'woodpc' ) . '</a>';
    wp_die(
        $message,
        $title,
        array( 'response' => 404 )
    ); // WPCS: XSS ok.
}
